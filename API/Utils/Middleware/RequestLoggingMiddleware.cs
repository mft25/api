﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace API.Utils.Middleware
{
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestLoggingMiddleware> _logger;

        // https://exceptionnotfound.net/using-middleware-to-log-requests-and-responses-in-asp-net-core/
        // https://github.com/exceptionnotfound/AspNetCoreRequestResponseMiddlewareDemo/blob/master/RequestResponseLoggingDemo.Web/Middleware/RequestResponseLoggingMiddleware.cs
        public RequestLoggingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<RequestLoggingMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            var request = await ExtractRequest(context.Request);

            var originalBodyStream = context.Response.Body;
            await using var responseBody = new MemoryStream();
            context.Response.Body = responseBody;

            try
            {
                await _next(context);
            }
            finally
            {
                var response = await ExtractResponse(context.Response);

                LogCompletedRequest(request, response);

                await responseBody.CopyToAsync(originalBodyStream);
            }
        }

        private async Task<RequestDetail> ExtractRequest(HttpRequest request)
        {
            request.EnableBuffering();

            var buffer = new byte[Convert.ToInt32(request.ContentLength)];

            await request.Body
                .ReadAsync(buffer, 0, buffer.Length)
                .ConfigureAwait(false);

            var bodyAsText = Encoding.UTF8.GetString(buffer);

            request.Body.Position = 0;

            return new RequestDetail
            {
                Scheme = request.Scheme,
                Host = request.Host.ToString(),
                Path = request.Path,
                QueryString = request.QueryString.ToString(),
                Method = request.Method,
                Body = bodyAsText,
            };
        }

        private async Task<ResponseDetail> ExtractResponse(HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            var text = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);

            return new ResponseDetail
            {
                StatusCode = response.StatusCode,
                Body = text,
            };
        }

        private void LogCompletedRequest(RequestDetail request, ResponseDetail response)
        {
            var level = response.StatusCode == (int)HttpStatusCode.InternalServerError
                ? LogLevel.Error
                : LogLevel.Information;

            _logger.Log(
                level,
                "{NewLine}{StatusCode}: {Method} {Path}{NewLine}{Request}{NewLine}{ResponseBody}",
                Environment.NewLine,
                response.StatusCode,
                request.Method,
                request.Path,
                Environment.NewLine,
                request,
                Environment.NewLine,
                response.Body);
        }
    }

    public class RequestDetail
    {
        public string Scheme { get; set; }
        public string Host { get; set; }
        public string Path { get; set; }
        public string QueryString { get; set; }
        public string Method { get; set; }
        public string Body { get; set; }

        public override string ToString()
        {
            return $"{Method} {Scheme}://{Host}{Path}{QueryString} {Body}";
        }
    }

    public class ResponseDetail
    {
        public int StatusCode { get; set; }
        public string Body { get; set; }
    }
}
